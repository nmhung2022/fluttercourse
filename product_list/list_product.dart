import 'package:flutter/material.dart';
import 'package:hello_flutter/product.dart';

class myapp6 extends StatelessWidget {
  const myapp6({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ListProduct(),
    );
  }
}

class ListProduct extends StatefulWidget {
  const ListProduct({Key? key}) : super(key: key);

  @override
  _ListProductState createState() => _ListProductState();
}

class _ListProductState extends State<ListProduct> {
  late Future<List<Product>> lsProduct;



  @override
  void initState() {
    lsProduct = Product.fetchData();
    print(lsProduct);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Product List")),
      body: FutureBuilder(
          future: lsProduct,
          builder:
              (BuildContext context, AsyncSnapshot<List<Product>> snapshot) {
            if (snapshot.hasData) {
              var data = snapshot.data;
              return ListView.builder(
                itemCount: data!.length,
                itemBuilder: (context, index) {
                  Product p = data[index];
                  return ListTile(
                    leading: Image.network(p.image),
                    title: Text(p.title),
                    trailing: IconButton(
                        onPressed: () => showButtonDialog(context),
                        color: Colors.blue,
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        icon: Icon(Icons.shopping_cart)
                    ),

                  );
                },
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }
}

showButtonDialog(BuildContext context) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      TextEditingController customController = TextEditingController();
      return AlertDialog(
        title: Text('Nhập số lượng mua:'),
        content: TextField(
          controller: customController,
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text('OK'),
          ),
        ],
      );
    },
  );
}
