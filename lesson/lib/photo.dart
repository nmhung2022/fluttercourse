import 'dart:convert';

import 'package:http/http.dart' as http;

class Photo {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  Photo(this.albumId, this.id, this.title, this.url, this.thumbnailUrl);

  static Future<List<Photo>> fetchData() async {
    String url = 'https://jsonplaceholder.typicode.com/photos/';
    var client = http.Client();

    var response = await client.get(Uri.parse(url));
    if (response.statusCode == 200) {

      var data = response.body;
      var jsonData = jsonDecode(data);
      List<Photo> ls = [];
      for (var item in jsonData) {
        var albumId = item['albumId'];
        var id = item['id'];
        var title = item['title'];
        var url = item['url'];
        var thumbnailUrl = item['thumbnailUrl'];

        Photo p = new Photo(albumId, id, title, url, thumbnailUrl);
        ls.add(p);
      }

      return ls;
    } else {
      print("Error");
      throw Exception("Bug here: ${response.statusCode}");
    }
  }
}
