import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hello_flutter/UserProfile/User.dart';
import 'package:http/http.dart' as http;

class UserProfile extends StatelessWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Hello world",
      home: Profile(),
    );
  }
}

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  bool _status = true;

  late Future<User> user;
  var txtFirst = TextEditingController();
  var txtLast = TextEditingController();
  var txtEmail = TextEditingController();
  var txtPhone = TextEditingController();
  var txtCell = TextEditingController();
  var txtGender = TextEditingController();
  var txtStreet = TextEditingController();
  var txtCity = TextEditingController();
  var txtState = TextEditingController();
  var txtCountry = TextEditingController();

  Padding paddingLabelOne(String textValue,
      {double pdLeft = 25.0, double pdRight = 25.0, double pdTop = 25.0}) {
    return Padding(
        padding: EdgeInsets.only(left: pdLeft, right: pdRight, top: pdTop),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  textValue,
                  style: TextStyle(fontSize: 16, color: Colors.black54),
                ),
              ],
            ),
          ],
        ));
  }

  Padding paddingInputOne(String textValue, TextEditingController txtController,
      String hint, bool disabled,
      {double pdLeft = 25.0, double pdRight = 25.0, double pdTop = 2.0}) {
    txtController.text = textValue;
    return Padding(
        padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 2.0),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            new Flexible(
              child: new TextField(
                controller: txtController,
                decoration: InputDecoration(
                  hintText: hint,
                ),
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
                enabled: disabled,
                autofocus: !_status,
              ),
            ),
          ],
        ));
  }

  Padding paddingLabelTwo(String textValueOne, String textValueTwo,
      {double pdLeft = 25.0, double pdRight = 25.0, double pdTop = 25.0}) {
    return Padding(
        padding: EdgeInsets.only(left: pdLeft, right: pdRight, top: pdTop),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Container(
                child: new Text(
                  textValueOne,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              flex: 2,
            ),
            Expanded(
              child: Container(

                child: new Text(
                  textValueTwo,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              flex: 2,
            ),
          ],
        ));
  }

  Padding paddingInputTwo(
      String textValueOne,
      TextEditingController txtControllerOne,
      String hintOne,
      bool disabledOne,
      String textValueTwo,
      TextEditingController txtControllerTwo,
      String hintTwo,
      bool disabledTwo,
      {double pdLeft = 25.0,
      double pdRight = 25.0,
      double pdTop = 2.0}) {
    txtControllerOne.text = textValueOne;
    txtControllerTwo.text = textValueTwo;
    return Padding(
        padding: EdgeInsets.only(left: pdLeft, right: pdRight, top: pdTop),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Flexible(
              child: Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: new TextField(
                  controller: txtControllerOne,
                  decoration: InputDecoration(hintText: hintOne),
                  enabled: disabledOne,
                ),
              ),
              flex: 2,
            ),
            Flexible(
              child: new TextField(
                controller: txtControllerTwo,
                decoration: InputDecoration(hintText: hintTwo),
                enabled: disabledTwo,
              ),
              flex: 2,
            ),
          ],
        ));
  }

  hexStringToColor(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return Color(int.parse(hexColor, radix: 16));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    user = fetchData();
  }

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = TextButton.styleFrom(
      textStyle: const TextStyle(fontSize: 20, color: Colors.blue),
    );
    return new Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white24,
        elevation: 0,
        title: Text(
          "Profile",
          style: TextStyle(fontSize: 28, color: Colors.black54),
        ),
        actions: <Widget>[
          TextButton(
            style: style,
            onPressed: () {},
            child: const Text('Save'),
          ),
        ],
      ),
      body: new FutureBuilder(
          future: user,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              var dataUser = snapshot.data as User;
              return ListView(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      new Container(
                        height: 170.0,
                        color: Colors.white,
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(top: 20.0),
                              child: new Stack(
                                  fit: StackFit.loose,
                                  children: <Widget>[
                                    new Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        new Container(
                                            width: 140.0,
                                            height: 140.0,
                                            decoration: new BoxDecoration(
                                              shape: BoxShape.circle,
                                              image: new DecorationImage(
                                                image: NetworkImage(dataUser
                                                    .results[0].picture.medium
                                                    .toString()),
                                                fit: BoxFit.cover,
                                              ),
                                            )),
                                      ],
                                    ),
                                  ]),
                            )
                          ],
                        ),
                      ),
                      new Container(
                        color: Color(0xffFFFFFF),
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 25.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              paddingLabelOne("Name"),
                              paddingInputOne(
                                dataUser.results[0].name.first,
                                txtFirst,
                                "Enter your name",
                                !_status,
                              ),
                              paddingLabelOne("Email"),
                              paddingInputOne(
                                dataUser.results[0].email,
                                txtEmail,
                                "Enter your email",
                                !_status,
                              ),
                              paddingLabelOne("Phone"),
                              paddingInputOne(
                                dataUser.results[0].phone,
                                txtPhone,
                                "Enter your phone",
                                !_status,
                              ),
                              paddingLabelTwo("City", "State"),
                              paddingInputTwo(
                                dataUser.results[0].location.city,
                                txtCity,
                                "Enter your City",
                                !_status,
                                dataUser.results[0].location.state,
                                txtState,
                                "Enter your State",
                                !_status,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }
          // margin: EdgeInsets.only(top: 5),
          // color: Colors.white,
          ),
      bottomNavigationBar: BottomAppBar(
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          child: Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.wallet_giftcard,
                      size: 30,
                      color: Colors.black45,
                    )),
                IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.access_time,
                      size: 30,
                      color: Colors.black45,
                    )),
                IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.supervised_user_circle_sharp,
                      color: Colors.amber,
                      size: 30,
                    )),
                IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.label,
                      size: 30,
                      color: Colors.black45,
                    )),
              ],
            ),
          )),
    );
  }

  static Future<User> fetchData() async {
    String url = "https://randomuser.me/api/";
    var client = http.Client();

    var response = await client.get(Uri.parse(url));
    if (response.statusCode == 200) {
      var result = response.body;
      var jsonData = jsonDecode(result);
      return User.fromJson(jsonData);
    } else {
      throw Exception("Failed to load UserProfile: ${response.statusCode}");
    }
  }
}
