import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

class MyApp3 extends StatelessWidget {
  const MyApp3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ListViewBasic(),
    );
  }
}

class ListViewBasic extends StatefulWidget {
  const ListViewBasic({Key? key}) : super(key: key);

  @override
  _ListViewBasicState createState() => _ListViewBasicState();
}

class _ListViewBasicState extends State<ListViewBasic> {
  var data = generateWordPairs().take(10);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("List view basic - Lecture 3")),
        body: Center(
            child: ListView.builder(
                itemCount: data.length,
                itemBuilder: (BuildContext context, int index) {
                  var wp = data.elementAt(index);
                  return Card(
                    elevation: 0,
                    child: Padding(
                      padding: EdgeInsets.all(5),
                      child: ListTile(
                        leading: CircleAvatar(
                            backgroundColor: Colors.indigo,
                            foregroundColor: Colors.green,
                            child: Text(
                              "${index + 1}",
                              style: TextStyle(
                                  fontSize: 20, color: Colors.deepOrangeAccent),
                            )),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            IconButton(
                              onPressed: () {},
                              icon: Icon(Icons.favorite_outline_rounded,
                                  color: Colors.indigoAccent),
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: Icon(Icons.facebook,
                                  color: Colors.indigoAccent),
                            ),
                          ],
                        ),
                        title: Text(
                          wp.asPascalCase,
                          style: TextStyle(fontSize: 30, color: Colors.red),
                        ),
                      ),
                    ),
                  );
                })));
  }
}
