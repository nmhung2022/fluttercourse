// import 'package:flutter/material.dart';
// import 'package:hello_flutter/product.dart';
// import 'package:carousel_slider/carousel_slider.dart';
//
// class myapp6 extends StatelessWidget {
//   const myapp6({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: ListProduct(),
//     );
//   }
// }
//
// class ListProduct extends StatefulWidget {
//   const ListProduct({Key? key}) : super(key: key);
//
//   @override
//   _ListProductState createState() => _ListProductState();
// }
//
// class _ListProductState extends State<ListProduct> {
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     lsProduct = Product.fetchData();
//   }
//
//   late Future<List<Product>> lsProduct;
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(title: Text("Product List")),
//         body: FutureBuilder(
//             future: lsProduct,
//             builder:
//                 (BuildContext context, AsyncSnapshot<List<Product>> snapshot) {
//               if (snapshot.hasData) {
//                 var data = snapshot.data;
//                 return ListView.builder(
//                   itemCount: data!.length,
//                   itemBuilder: (BuildContext context, int index) {
//                     child:
//                     Center(
//                       child: CarouselSlider(
//                           options: CarouselOptions(
//                             height: 400,
//                             aspectRatio: 16 / 9,
//                             viewportFraction: 0.8,
//                             initialPage: 0,
//                             enableInfiniteScroll: true,
//                             reverse: false,
//                             autoPlay: true,
//                             autoPlayInterval: Duration(seconds: 3),
//                             autoPlayAnimationDuration: Duration(
//                                 milliseconds: 800),
//                             autoPlayCurve: Curves.fastOutSlowIn,
//                             enlargeCenterPage: true,
//                             scrollDirection: Axis.horizontal,
//                           ),
//                           items: data.map((p) =>
//                               ClipRect(
//
//                               )).toList()
//
//                       ),
//                     );
//                   },
//
//                 );
//               }
//             }
//         )
//
//     );
//   }
//
//   showButtonDialog(BuildContext context) {
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         TextEditingController customController = TextEditingController();
//         return AlertDialog(
//           title: Text('Số lượng:'),
//           content: TextField(
//             controller: customController,
//           ),
//           actions: <Widget>[
//             TextButton(
//               onPressed: () => Navigator.pop(context, 'Huỷ'),
//               child: const Text('Huỷ'),
//             ),
//             TextButton(
//               onPressed: () => Navigator.pop(context, 'Đồng ý'),
//               child: const Text('Đồng ý'),
//             ),
//           ],
//         );
//       },
//     );
//   }
