import 'dart:convert';

import 'package:hello_flutter/rating.dart';
import 'package:http/http.dart' as http;

class Product {
  final int id;
  final String title;
  final double price;
  final String description;
  final String category;
  final String image;
  final Rating rating;

  Product(this.id, this.title, this.price, this.description, this.category,
      this.image, this.rating);

  static Future<List<Product>> fetchData() async {
    String url = "https://fakestoreapi.com/products";
    var client = http.Client();
    var response = await client.get(Uri.parse(url));
    print(response.statusCode);
    if (response.statusCode == 200) {
      print("Lay du lieu thanh cong");
      var result = response.body;
      var jsonData = jsonDecode(result);
      List<Product> ls = [];
      for (var item in jsonData) {
        var id = item['id'];
        var title = item['title'];
        dynamic price = item['price'];
        var description = item['description'];
        var category = item['category'];
        var image = item['image'];
        dynamic rate = item['rating']['rate'];
        dynamic count = item['rating']['count'];
        Rating rating = new Rating(rate, count);
        Product p = new Product(id, title, price, description, category,
            image, rating);
        ls.add(p);
      }
      return ls;
    } else {
      throw Exception("Loi lay du lieu");
    }
  }
}

