import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AppBaiTap extends StatelessWidget {
  const AppBaiTap({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BaiTap(),
    );
  }
}
class BaiTap extends StatefulWidget {
  const BaiTap({Key? key}) : super(key: key);

  @override
  _BaiTapState createState() => _BaiTapState();
}

class _BaiTapState extends State<BaiTap> {
  late Future<List<Product>> lsproduct;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    lsproduct = Product.fetchData();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: lsproduct,
        builder:(BuildContext context,
            AsyncSnapshot<dynamic> snapshot){
          if(snapshot.hasData){
            List<Product> data = snapshot.data;
            return ListView.builder(
                itemCount:data.length,
                itemBuilder:(context,index){
                  var product = data[index];
                  return ListTile(
                    leading:Image.network(product.image),
                    title: Text(product.title),
                    trailing: IconButton(onPressed: () => showButtonDialog(context),
                        color: Colors.blue,
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        icon: Icon(Icons.shopping_cart)),);
                });
          }
          else
            return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

showButtonDialog(BuildContext context) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      TextEditingController customController = TextEditingController();
      return AlertDialog(
        title: Text('Nhập số lượng mua:'),
        content: TextField(
          controller: customController,
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text('OK'),
          ),
        ],
      );
    },
  );
}

class Rating {
  final double rate;
  final int count;

  Rating(this.count, this.rate);
}

class Product {
  final int id;
  final String title;
  final double price;
  final String description;
  final String category;
  final String image;
  final Map rating;

  Product(this.id, this.title, this.price, this.description, this.category,
      this.image, this.rating);

  static Future<List<Product>> fetchData() async {
    String url = "https://fakestoreapi.com/products/2";
    var client = http.Client();
    var response = await client.get(Uri.parse(url));
    List<Product> lsProduct = [];
    if (response.statusCode == 200) {
      var result = response.body;
      print(result);
      var jsonData = jsonDecode(result);
      for (var item in jsonData) {
        var id = item['id'];
        var title = item['title'];
        var price = item['price'];
        var description = item['description'];
        var category = item['category'];
        var image = item['image'];
        var rating = item['rating'];
        Product p =
            new Product(id, title, price, description, category, image, rating);
        lsProduct.add(p);
      }
      return lsProduct;
    } else {
      print(response.statusCode);
      throw Exception("Loi lay du lieu");
    }
  }
}
