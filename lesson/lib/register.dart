import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hello_flutter/login.dart';

class myapp7 extends StatelessWidget {
  const myapp7({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Register(),
    );
  }
}

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  var fKey = GlobalKey<FormState>();
  var txtUserName = TextEditingController();
  var txtPassword = TextEditingController();
  var txtRePassword = TextEditingController();
  var txtPhoneNumber = TextEditingController();
  bool _passwordVisible = false;
  bool value = false;

  @override
  void initState() {
    _passwordVisible = false;
  }

  void launchWebView() {
    print("1234");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: fKey,
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Text(
              "Register",
              style: TextStyle(fontSize: 20, color: Colors.red),
            ),
            TextFormField(
              controller: txtUserName,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Email is required';
                } else if (RegExp(
                        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                    .hasMatch(value)) {
                  return null;
                } else {
                  return "Email is not valid";
                }
              },
              decoration: InputDecoration(
                  icon: Icon(Icons.person),
                  hintText: "Enter your email adress",
                  labelText: "Email"),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              obscureText: !_passwordVisible,
              controller: txtPassword,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "Password is required";
                } else if (value.length < 8) {
                  return "password must be at least 8 characters";
                } else {
                  return null;
                }
              },
              decoration: InputDecoration(
                  icon: Icon(Icons.lock),
                  hintText: "Enter your password",
                  labelText: "Password",
                  suffixIcon: IconButton(
                    icon: Icon(
                      // Based on passwordVisible state choose the icon
                      _passwordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: Theme.of(context).primaryColorDark,
                    ),
                    onPressed: () {
                      setState(() {
                        _passwordVisible = !_passwordVisible;
                      });
                    },
                  )),
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              obscureText: !_passwordVisible,
              controller: txtRePassword,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "Password is required";
                } else if (value.length < 8) {
                  return "password must be at least 8 characters";
                } else {
                  return null;
                }
              },
              decoration: InputDecoration(
                  icon: Icon(Icons.lock),
                  hintText: "Enter your re-password",
                  labelText: "RePassword",
                  suffixIcon: IconButton(
                    icon: Icon(
                      // Based on passwordVisible state choose the icon
                      _passwordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: Theme.of(context).primaryColorDark,
                    ),
                    onPressed: () {
                      setState(() {
                        _passwordVisible = !_passwordVisible;
                      });
                    },
                  )),
            ),

            SizedBox(
              height: 20,
            ),
            Row(
              children: <Widget>[
                SizedBox(
                  width: 10,
                ), //SizedBox
                Column(
                  children: [
                    Row(
                      children: [
                        Checkbox(
                          value: this.value,
                          onChanged: (value) => setState(() {
                            this.value = value!;
                          }),
                        ), //Checkbox
                        Text(
                          "By creating an account, you agree to the Terms of Service.",
                          style: TextStyle(fontSize: 12.0),
                        ),
                      ],
                    ),
                  ],
                ), //Text
              ], //<Widget>[]
            ),
            //Row
            ElevatedButton(
              onPressed: () {
                if (fKey.currentState!.validate()) {
                  var username = txtUserName.text;
                  var password = txtPassword.text;
                  var rePassword = txtRePassword.text;
                  if (password.compareTo(rePassword) == 0 && value == true) {
                    var alert = AlertDialog(
                      content: Text(
                        "Register successful you can login now",
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'Cancel'),
                          child: const Text('Cancel'),
                        ),
                        TextButton(
                          onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Login()),
                          ),
                          child: const Text('Login'),
                        ),
                      ],
                    );
                    showDialog(
                        context: context,
                        builder: (context) {
                          return alert;
                        });
                  } else {
                    var alert = AlertDialog(
                        content: Text(
                          "Register fail",
                        ),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text('Cancel'),
                          ),
                        ]);
                    showDialog(
                        context: context,
                        builder: (context) {
                          return alert;
                        });
                  }
                }
              },
              child: Text("Register"),
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Login()),
                  );
                },
                child: Text("Login")),
          ],
        ),
      ),
    );
  }
}
